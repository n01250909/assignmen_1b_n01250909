﻿using System;
using System.Web;
using System.Web.UI;

namespace Assignment_1b_n01250909
{
    public partial class Default : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs eventArgs)
        {

        }

        protected void ConfirmBooking(object sender, EventArgs e)
        {

            //client
            string initials = clientInitials.Text.ToString();
            string fname = clientfName.Text.ToString();
            string lname = clientlName.Text.ToString();
            string phone = clientPhone.Text.ToString();
            string email = clientEmail.Text.ToString();

            Client newclient = new Client();
            newclient.ClientInitials = initials;
            newclient.ClientlName = lname;
            newclient.ClientfName = fname;
            newclient.ClientPhone = phone;
            newclient.ClientEmail = email;

            //hotel
            string type = roomType.SelectedItem.Text.ToString();
            string option = levelOption.SelectedItem.Text.ToString();
            string service = service1.Text.ToString();

            Hotel newhotel = new Hotel(type, option, service);


            //booking
            string checkin = checkIn.Text.ToString();
            string checkout = checkOut.Text.ToString();

            Booking newbooking = new Booking(checkin, checkout);

            //confirmBooking
            ConfirmBooking newconfirmbooking = new ConfirmBooking(newclient, newhotel, newbooking);

            ConfirmBookingRes.InnerHtml = newconfirmbooking.BookingConfirmation();


        }
    }
}

         
                