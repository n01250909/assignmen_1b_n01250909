﻿using System;
namespace Assignment_1b_n01250909
{
    public class Client
    {
        private string clientInitials;
        private string clientfName;
        private string clientlName;
        private string clientPhone;
        private string clientEmail;

        public Client()
        {

        }
        public string ClientInitials
        {
            get { return clientInitials; }
            set { clientInitials = value; }
        }

        public string ClientfName
        {
            get { return clientfName; }
            set { clientfName = value; }
        }

        public string ClientlName
        {
            get { return clientlName; }
            set { clientlName = value; }
        }

        public string ClientPhone
        {
            get { return clientPhone; }
            set { clientPhone = value; }
        }

        public string ClientEmail
        {
            get { return clientEmail; }
            set { clientEmail = value; }
        }
    }
}
