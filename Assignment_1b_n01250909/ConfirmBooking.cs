﻿using System;
namespace Assignment_1b_n01250909
{
    public class ConfirmBooking
    {
        public Client client;
        public Hotel hotel;
        public Booking booking;

        public ConfirmBooking(Client c, Hotel h, Booking b)
        {
            client = c;
            hotel = h;
            booking = b;
        }

        public string BookingConfirmation()
        {
            string confirmation = "BOOKING CONFIRMATION: " + " <br/> ";

            confirmation += "Initials: " + client.ClientInitials.ToString() + "<br/>";
            confirmation += "Name: " + client.ClientfName.ToString() + " " + client.ClientlName.ToString() + "<br/>";
            confirmation += "Phone Number: " + client.ClientPhone.ToString() + "<br/>";
            confirmation += "Email: " + client.ClientEmail.ToString() + "<br/>" + "<br/>";

            confirmation += " ** BOOKING INFORMATION ** " +  "<br/>";
            confirmation += "Room Type: " + hotel.roomType.ToString() + "<br/>";
            confirmation += "Check In Date: " + booking.checkIn.ToString() + "<br/>";
            confirmation += "Check Out Date: " + booking.checkOut.ToString() + "<br/>";
            confirmation += "Level: " + hotel.levelOption.ToString() + "<br/>";
            confirmation += "Options: " + String.Join("; ", hotel.service.ToString()) + "<br/>" + "<br/>";

            confirmation += " ** TOTAL ** " + "<br/>";
            confirmation += "Your price for one night will be: " + BookingSum().ToString() + "$" + "<br/>" +  "<br/>";

            confirmation += " ** WE WILL BE HAPPY TO SEE YOU ** ";
            return confirmation;

        }

        public double BookingSum()
        {
            double total = 0;
            if (hotel.roomType == "Single")
            {
                total = 90;
            }
            else if (hotel.roomType == "Double")
            {
                total = 110;
            }
            else if (hotel.roomType == "Queen")
            {
                total = 130;
            }
            else if (hotel.roomType == "King")
            {
                total = 150;
            }

            return total;
        }
    }
}