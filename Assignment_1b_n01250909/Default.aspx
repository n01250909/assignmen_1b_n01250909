﻿<%@ Page Language="C#" Inherits="Assignment_1b_n01250909.Default" %>
<!DOCTYPE html>
<html>
<head runat="server">
    <title>Room Reservation</title>
</head>
<body>
    <form id="form" runat="server">
        <div>
            <p>HOTEL ROOM RESERVATION</p>
                <asp:DropDownList runat="server" ID="clientInitials">
                <asp:ListItem Value="Mr." Text="Mr."></asp:ListItem>
                <asp:ListItem Value="Ms." Text="Ms."></asp:ListItem>
                <asp:ListItem Value="Mrs." Text="Mrs."></asp:ListItem>
            </asp:DropDownList>
            <br /> <br /> 
                <label>First Name:</label><br />
                <asp:TextBox runat="server" ID="clientfName"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter a First Name" ControlToValidate="clientfName" ID="validatorfName"> </asp:RequiredFieldValidator>
            <br /> <br /> 
                <label>Last Name:</label><br />
                <asp:TextBox runat="server" ID="clientlName"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter a Last Name" ControlToValidate="clientlName" ID="validatorlName"> </asp:RequiredFieldValidator>
            <br /> <br /> 
                <label>Phone Number:</label><br />
                <asp:TextBox runat="server" ID="clientPhone"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter a Phone Number" ControlToValidate="clientPhone" ID="regexPhoneValid"></asp:RequiredFieldValidator>
                <asp:CompareValidator runat="server" ControlToValidate="clientPhone" Type="String" Operator="NotEqual" ValueToCompare="^\(?([0-9]{3})\)?[-.●]?([0-9]{3})[-.●]?([0-9]{4})$" ErrorMessage="This is an invalid phone number. Please try again"></asp:CompareValidator>
            <br /> <br />
                <label>Email:</label><br />
                <asp:TextBox runat="server" ID="clientEmail" placeholder="example@gmail.com"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter an Email" ControlToValidate="clientEmail" ID="RequiredFieldValidator1"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="regexEmailValid" runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="clientEmail" ErrorMessage="Invalid Email Format"></asp:RegularExpressionValidator>
            <br /> <br />
                <label>Check In:</label><br />
                <asp:TextBox runat="server" ID="checkIn" placeholder="dd.mm.yy"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ControlToValidate="checkIn" ErrorMessage="Please enter Ckeck In date"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="regexDateValid" runat="server" ValidationExpression="^[0-3]?[0-9].[0-3]?[0-9].(?:[0-9]{2})?[0-9]{2}$" ControlToValidate="checkIn" ErrorMessage="Invalid Date Format"></asp:RegularExpressionValidator>
            <br /> <br />
                <label>Check Out:</label><br />
                <asp:TextBox runat="server" ID="checkOut" placeholder="dd.mm.yy"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ControlToValidate="checkOut" ErrorMessage="Please enter Check Out date"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="regexDateValid1" runat="server" ValidationExpression="^^[0-3]?[0-9].[0-3]?[0-9].(?:[0-9]{2})?[0-9]{2}$" ControlToValidate="checkOut" ErrorMessage="Invalid Date Format"></asp:RegularExpressionValidator>
            <br /> <br />
                <label>Choose a room:</label><br />
                <asp:RadioButtonList runat="server" ID="roomType">
                <asp:ListItem runat="server" Text="Single"></asp:ListItem>
                <asp:ListItem runat="server" Text="Double"></asp:ListItem>
                <asp:ListItem runat="server" Text="Queen"></asp:ListItem>
                <asp:ListItem runat="server" Text="King"></asp:ListItem>
                </asp:RadioButtonList>
            <br /> 
                <label>Level</label><br />
                <asp:DropDownList runat="server" ID="levelOption">
                <asp:ListItem Value="1 - 5" Text="1  - 5"></asp:ListItem>
                <asp:ListItem Value="6 - 10" Text="6 - 10"></asp:ListItem>
                <asp:ListItem Value="11 - 12" Text="11 - 12"></asp:ListItem>
                </asp:DropDownList>
            <br /> <br />
                <label>Optional:</label><br />
                <asp:CheckBox runat="server" ID="service1" Text="Airport Pick-up"/>
                <asp:CheckBox runat="server" ID="service2" Text="Airport Drop-off"/>     
            <br /> <br />
                <asp:Button runat="server" ID="confirmButton" OnClick="ConfirmBooking" Text="Confirm a booking"/>
            <br /> <br />
               
                <div runat="server" id="ClientRes"> </div>
                
                <div runat="server" id="HotelRes"> </div>
                
                <div runat="server" id="BookingRes"> </div> 
                
                <div runat="server" id="ConfirmBookingRes"></div>
  
        </div>
    </form>
</body>
</html>
