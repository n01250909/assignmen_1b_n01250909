﻿using System;
namespace Assignment_1b_n01250909
{
    public class Hotel
    {
        public string roomType;
        public string levelOption;
        public string service;

        public Hotel(string rt, string lo, string s)
        {
            roomType = rt;
            levelOption = lo;
            service = s;
        }
    }
}
